#define PINO_LDR    0     // pino analógico

char comando;

void setup()
{
    Serial.begin(9600);
}

void loop(){
    if (Serial.available())
    {
        comando = Serial.read();
        if (comando == 'l') 
           Serial.println(ler_LDR());
        else
           Serial.println("<comando_invalido>");
    }
}

float ler_LDR()
{
    return 100/1023.0 * analogRead(PINO_LDR);
}
