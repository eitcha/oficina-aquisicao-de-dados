#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Controlador da estação meteorologica
# Por Rafael Pezzi
#  Centro de Tecnologia Acadêmica - UFRGS
#  http://cta.if.ufrgs.br
#
# Licença: GPL v3
#
# Baseado em projeto de Gilson Giuriatti
#  https://github.com/gilsong/labduino


import time, serial, datetime, os

DATALOG_FILENAME = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                               'dados/meteorolog.log')
OPLOG_FILENAME = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                              'dados/oplog.log')
SERIAL_PORT = '/dev/ttyACM0'
BAUD_RATE = 9600

ser = serial.Serial(SERIAL_PORT, BAUD_RATE)
time.sleep(2)

meteorologfile = open(OPLOG_FILENAME, 'a')
now=datetime.datetime.now()
meteorologfile.write(now.strftime("%Y-%m-%d %H:%M:%S")+" - Iniciando log meteorológico\n")
meteorologfile.close()

print "Iniciando Log da Estação Meterológica"

u_s = '668'

while True:
  try:
    ser.write('t')
    t = float(ser.readline().replace('\r\n',''))
  except KeyboardInterrupt:
    break

  try:
    ser.write('u')
    u_ar = float(ser.readline().replace('\r\n',''))
  except KeyboardInterrupt:
    break

  try:
    ser.write('l')
    lum = float(ser.readline().replace('\r\n',''))
  except KeyboardInterrupt:
    break

  try:
    ser.write('p')
    pressao = int(ser.readline().replace('\r\n',''))
    now=datetime.datetime.now()
    logfile = open(DATALOG_FILENAME, 'a')
    print now.strftime("%Y%m%d%H%M%S"),"\t",  t,"\t", u_ar,"\t", u_s, "\t", pressao, "\t", lum
    logfile.write(now.strftime("%Y%m%d%H%M%S")+"\t"+str(t)+"\t"+str(u_ar)+"\t"+str(u_s)+"\t"+str(pressao)+"\t"+str(lum)+'\n')
    logfile.close()
    time.sleep(5)
  except KeyboardInterrupt:
    brea


ser.close();

