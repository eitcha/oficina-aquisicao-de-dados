# -*- coding:utf-8 -*-
# Programa de visualização e estatísticas de dados meteorológicos 
# coletados pela estação Meteorolog do Centro de Tecnologia Acadêmica
# Dados no formato DataHora Temperatura Umidade_do_Ar Solo Pressão Luminosidade 
# DataHora no formato  AAAAMMDDHHmmSS onde
#          AAAA = Ano
#          MM = Mês
#          DD = Dia
#          HH = Hora
#          mm = Minuto
#          ss = Segundo
#
# Mais informações:
#  http://cta.if.ufrgs.br/projects/estacao-meteorologica-modular/wiki/Meteorolog
#
# Autor: Rafael Pezzi - Centro de Tecnologia Acadêmica - IF/UFRGS
#
# Este programa é softare livre, disponível sob os termos da
# GNU Affero General Public License V3 
# http://www.gnu.org/licenses/agpl-3.0.html

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import random as random
import datetime as datetime
import sys , os

print "Estatísticas Meteorológicas Meteorolog - Centro de Tecnologia Acadêmica"

# Testa se o número de argumentos está correto 
if len(sys.argv) < 2:
    sys.stderr.write('Uso: python '+ sys.argv[0]+' arquivo_de_log\n' )
    sys.exit(1)
# e se o arquivo de log existe
if not os.path.exists(sys.argv[1]):
    sys.stderr.write('ERRO: Arquivo '+sys.argv[1]+' não encontrado!\n')
    sys.exit(1)

# Ativa/desativa seleção de intervalo
# Caso negativo (False), todos dados do arquivo de log serão computados
#seleciona_intervalo = False
seleciona_intervalo = False

# Defina aqui o intervalo de tempo a se analisado
#  TODO: Incluir seleção pela linha de comando de execução  
sel_Inicio = datetime.datetime(2013,03,01,00,00,00)
sel_Fim = datetime.datetime(2014,03,10,23,59,59)

# Armazena os dados coletados em matrizes unidimensionais
data, temp, u_ar, u_solo, pressao, lum = np.loadtxt(sys.argv[1], unpack=True,
        converters={ 0: mdates.strpdate2num('%Y%m%d%H%M%S')})

## Seria útil incluir um algoritmo de filtragem para remover pontos expurios 
##   observados em algumas partes por mil medidas de temperatura

# Exibe informações dos dados
print "Dados de "+str(mdates.num2date(data[0]).strftime('%Y-%m-%d %H:%M:%S'))+u" até "+str(mdates.num2date(data[len(data)-1]).strftime('%Y-%m-%d %H:%M:%S'))
print "Série com "+str(len(data))+" amostras\n"

# Seleciona dados em um intervalo de tempo
if (seleciona_intervalo == True):

    # Cria listas temporárias para seleção
    # Acho que pode ser otimizado, usando iteradores direto em arrays
    #  crítico para series de dados gigantes
    selec_data=[]; selec_temp=[];  selec_u_ar=[] 
    selec_u_solo=[]; selec_pressao=[]; selec_lum=[] 
    for j in enumerate(data):
        if mdates.date2num(sel_Inicio) <= j[1] <= mdates.date2num(sel_Fim):
            selec_data.append(data[j[0]])
            selec_temp.append(temp[j[0]])
            selec_u_ar.append(u_ar[j[0]])
            selec_u_solo.append(u_solo[j[0]])
            selec_pressao.append(pressao[j[0]])
            selec_lum.append(lum[j[0]])

    # Copia dados selecionados para matrizes originais
    data=np.asarray(selec_data)
    temp=np.asarray(selec_temp)
    u_ar=np.asarray(selec_u_ar)
    u_solo=np.asarray(selec_u_solo)
    pressao=np.asarray(selec_pressao)
    lum=np.asarray(selec_lum)

    # Exibe informações sobre o periodo selecionado
    print "Selecionado período:"
    print str(sel_Inicio)+" até "+str(sel_Fim)
    print "Série com "+str(len(data))+" amostras\n"

## Análise da temperatura
T_min=temp.min()
T_max=temp.max()
T_med=temp.mean()
T_std=temp.std()
print "Temperatura: \nmin: "+str(T_min)+"°C\tmax: "+str(T_max)+"°C\tmédia: "+str("%.2f" % T_med)+"°C\tDesvio: "+str("%.1f" % T_std)+"°C"

## Análise da umidade relativa do Ar
U_ar_min=u_ar.min()
U_ar_max=u_ar.max()
U_ar_med=u_ar.mean()
U_ar_std=u_ar.std()
print "\nUmidade Relativa do Ar: \nmin: "+str(U_ar_min)+"%\tmax: "+str(U_ar_max)+"%\tmédia: "+str("%.1f" % U_ar_med)+"%\tDesvio: "+str("%.1f" % U_ar_std)+"%"

# Análise da pressão atomsférica
P_min=pressao.min()
P_max=pressao.max()
P_med=pressao.mean()
P_std=pressao.std()

print "\nPressão: \nmin: "+str(int(P_min))+" Pa\tmax: "+str(int(P_max))+" Pa\tmédia: "+ str("%.0f" % P_med)+" Pa\tDesvio: "+str("%.2f" % P_std)+" Pa"

# Análise da luminosidade
L_min=lum.min()
L_max=lum.max()
L_med=lum.mean()
L_std=lum.std()

print "\nLuminosidade: \nmin: "+str(int(L_min))+" u.a.\tmax: "+str(int(L_max))+" u.a.\tmédia: "+ str("%.0f" % L_med)+" u.a.\tDesvio: "+str("%.2f" % L_std)+" u.a."


# Apresentação de histrograma:
# Temperatura
plt.figure() # Cria nova figura
plt.ylabel(u"Número de eventos")
plt.xlabel(u"Temperatura (°C)")
plt.hist(temp,bins=25)
#Inclui título com média e desvio padrão
#plt.text(plt.xlim()[0]+0.5,0.9*plt.ylim()[1],u"Média: "+str("%.2f" % temp.mean())+u"°C\nDesvio Padrão: "+str("%.2f" % temp.std())+u"°C") # Este coloca dentro da figura
plt.title(u"Temperatura média: "+str("%.2f" % T_med)+u"°C  |  Desvio Padrão: "+str("%.2f" % temp.std())+u"°C")
plt.grid(True)

# Apresentação de histrograma:
# Umidade Rel. do Ar
plt.figure()
plt.ylabel(u"Número de eventos")
plt.xlabel("Umidade Relativa (%)")
plt.hist(u_ar,bins=range(26,100,2))
#plt.text(plt.xlim()[0]+1,0.9*plt.ylim()[1],u"Média: "+str("%.2f" % u_ar.mean())+u" %\nDesvio Padrão: "+str("%.2f" % u_ar.std())+" %")
plt.title(u"UR média: "+str("%.2f" % u_ar.mean())+u" %  |  Desvio Padrão: "+str("%.2f" % u_ar.std())+" %")
plt.grid(True)

# Apresentação de histrograma:
# Pressão
plt.figure()
plt.ylabel(u"Número de eventos")
plt.xlabel(u"Pressão atmosférica (Pa)")
plt.hist(pressao,bins=25)
#plt.text(1.001*plt.xlim()[0],0.9*plt.ylim()[1],u"Média: "+str("%.2f" % pressao.mean())+u" Pa\nDesvio Padrão: "+str("%.2f" % pressao.std())+" Pa")
plt.title(u"Pressão média: "+str("%.2f" % pressao.mean())+u" Pa  |  Desvio Padrão: "+str("%.2f" % pressao.std())+" Pa")
plt.grid(True)

# Apresentação de histrograma:
# Luminosidade
plt.figure()
plt.ylabel(u"Número de eventos")
plt.xlabel(u"Luminosidade (u.a.)")
plt.hist(lum,bins=25)
#plt.text(1*plt.xlim()[0]+60,0.9*plt.ylim()[1],u"Média: "+str("%.2f" % lum.mean())+u"\nDesvio Padrão: "+str("%.2f" % lum.std())+" ")
plt.title(u"Luminosidade média: "+str("%.2f" % lum.mean())+u"  |  Desvio Padrão: "+str("%.2f" % lum.std())+" ")
plt.grid(True)

## Apresentação das series temporais de medidas
# Temperatura
plt.figure()
# Gira o texto do eixo temporal
plt.xticks(rotation=70)
plt.plot_date(x=data, y=temp, fmt="r.")

# Define os limites temporais do gráfico 
#   útil para evitar escalas exageradas devido a dadas incorretas - ocorreu na estação sem relógio
plt.xlim(data[0], data[len(data)-1])
plt.ylabel(u"Temperatura (°C)")
plt.grid(True)
# Altera as margens para a margem inferior comportar a data
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

# Umidade Relativa do Ar
plt.figure()
plt.xticks(rotation=70)
plt.plot_date(x=data, y=u_ar, fmt="r.")
plt.xlim(data[0], data[len(data)-1])
plt.ylabel("Umidade relativa do ar (%)")
plt.grid(True)
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)

'''
# Condutividade do solo
plt.figure()
plt.xticks(rotation=70)
plt.plot_date(x=data, y=u_solo, fmt="r.")
plt.xlim(data[0], data[len(data)-1])
plt.ylabel("Umidade do solo (u.a.)")
plt.grid(True)
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)
'''

# Pressão
plt.figure()
plt.xticks(rotation=70)
plt.plot_date(x=data, y=pressao, fmt="r.")
plt.xlim(data[0], data[len(data)-1])
plt.ylabel(u"Pressão atmosférica (Pa)")
plt.grid(True)
plt.subplots_adjust(left=0.13, right=0.9, top=0.9, bottom=0.2)

# Luminosidade
plt.figure()
plt.xticks(rotation=70)
plt.plot_date(x=data, y=lum, fmt="r.")
plt.xlim(data[0], data[len(data)-1])
plt.ylabel("Luminosidade (u.a.)")
plt.grid(True)
plt.subplots_adjust(left=0.1, right=0.9, top=0.9, bottom=0.2)




#'''

#Apresenta os gráficos gerados
plt.show()


