#define PINO_LDR    0     // pino analógico

void setup()
{
    Serial.begin(9600);
}

void loop()
{
    Serial.println(ler_LDR());
    delay(500);
}

float ler_LDR()
{
    return 100/1023.0 * analogRead(PINO_LDR);
}
