#define PINO_LDR    0     // pino analógico
#define PINO_DHT22  4     // pino digital

#include "DHT.h"
DHT dht(PINO_DHT22, DHT22);
char comando;

void setup(){
    Serial.begin(9600);
    dht.begin();
}

void loop(){
    if (Serial.available())
    {
        comando = Serial.read();
        if (comando == 'l') 
           Serial.println(ler_LDR());
        else if (comando == 't') 
           Serial.println(dht.readTemperature());
        else if (comando == 'u') 
           Serial.println(dht.readHumidity());
        else
           Serial.println("<comando_invalido>");
    }
}

float ler_LDR()
{
    return 100/1023.0 * analogRead(PINO_LDR);
}
