#define PINO_LDR    0     // pino analógico
#define PINO_DHT22  4     // pino digital

#include "Wire.h"

#include "DHT.h"
DHT dht(PINO_DHT22, DHT22);
char comando;

#include "Adafruit_BMP085.h"
Adafruit_BMP085 bmp;

void setup()
{
    Serial.begin(9600);
    dht.begin();
    bmp.begin();
}

void loop()
{
    if (Serial.available())
    {
        comando = Serial.read();

        if (comando == 'l') 
           Serial.println(ler_LDR());         

        else if (comando == 't')                 
           Serial.println(dht.readTemperature());

        else if (comando == 'u') 
           Serial.println(dht.readHumidity());

        else if (comando == 'p') 
           Serial.println(bmp.readPressure());

        else if (comando == 'T') 
           Serial.println(bmp.readTemperature());

        else if (comando == 'a') 
           Serial.println(bmp.readAltitude());

        else
           Serial.println("<comando_invalido>");
    }
}

float ler_LDR()
{
    return 100/1023.0 * analogRead(PINO_LDR);
}
