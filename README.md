Oficina Monitoramenteo Ambiental Colaborativo e Aquisição de Dados Meteorológicos
============================================

Está atividade é composta de duas partes. Na parte I, chamada "Monitoramento Ambiental Colaborativo", contamos a história do movimento das tecnologias livres, começando com Stallman, passando por Santos Dumont e chegando nos dias atuais com o Global Open Scince Hardware. Além disso, realizamos algumas atividades de origami e relacionamos isso com a importância da documentação. Tudo isso envolvido na temática do monitoramento ambiental. A parte II é uma oficina prática, chamada Aquisição de Dados Meteorológicos, na qual as participantes irão montar uma estação meteorológica numa protoboaard e visualizar os dados. Nesta oficina usamos a plataforma Arduino e são trabalhadas noções básicas de programação e eltrônica 

Esta oficina comprrende o estudo e desenvolvimento de instrumentação científica e educacional de código aberto, de baixo custo com vistas à formação de uma rede de ciência cidadã para pesquisas climáticas e ambientais. Essa oficina é pensada àqueles que tem interesse em se aproximar do projeto ou em avançar sua habilidade com o Arduino.

Oficina da [EITCHA](http://eitcha.org),

A parte I da oficina tem a duração de *2h* e a parte II de *2h*.

O objetivo desse material é apresentar uma oficina completa e auto-contida, que não precise de muitos outros recursos para funcionar. Ela pode ser oferecida em conjunto com a oficinas de introdução ao Arduino.

### Softwares utilizados: ###

* IDE do Arduino
* Python 3

Conteúdo
==============
* Monitoramento ambiental;
* História do movimento das tecnologias livres;
* Programação na Plataforma Arduino;
* Eletrônica Básica;
* Noções básicas de Linux
* Aquisição de dados de sensores.
* Sensores de grandezas atmosféricas;

*###* 

Lista de Materiais
============


*Monitoramento Ambiental Colaborativo*

* de 6 a 10 folhas A4;
* pelo menos 4 tutoriais para montagem do avião (disponível no respositório);

*Aquisição de Dados Meteorológicos*
(Por dupla de participantes)
* 1 placa Arduino;
* 1 protoboard;
* 1 LDR;
* 1 DHT022 ou DHT011;
* 1 BMP085;
* 1 resistor de 4k7 Ω;
* 1 resistor de 2k2 Ω;
* 5 jumpers longos;
* 9 jumpers curtos;
* 1 penDrive com sistema operacional TropOS;

Infraestrutura
=========================

E necessário um computador para cada 2 participantes. No caso do computador ser alguma distribuição linux, é preciso instalar a IDE do Arduino e Python 3. Caso contrário, é preciso que o computador seja bootável, ou seja, aceite rodar um sistema operacional através de um pendrive.


